#!/bin/env python3.6

import argparse
import os
import sys
import math
import mp7
import mp7.tools.data as data
from  mp7 import BoardDataFactory, BoardDataWriter
import numpy

# map to link each MP tx links to demux rx links 
# each set of 6 corresponds to an MP
# 6 links in each set are the MP tx links 60-65
linkMap = [[6,0,18,12,30,24],[7,1,19,13,31,25],[8,2,20,14,32,26],
           [9,3,21,15,33,27],[10,4,22,16,34,28],[60,66,48,54,36,42],
           [61,67,49,55,37,43],[62,68,50,56,38,44],[63,69,51,57,39,45]]

# define commas and headers
comma = mp7.Frame()
comma.valid = 0
comma.data  = 0x505050bc
head = mp7.Frame()
head.valid = 1
firstHead = head

# frame index of the first demux rx header on link 0
dmOffset = 12


def generateDemuxRx(txBuffers, outDir):
    
    print('Generating Demux rx file')
    rxBuf = mp7.BoardDataFactory.generate('generate://empty')
    #rxBuf.name = 'DEMUX'

    # loop over MPs/buffer files
    for txBuf in txBuffers:
        mp = txBuffers.index(txBuf)
        # loop over MP tx links
        for link in range(60,66):
            event = 0
            # find the demux rx link that receives data from a given MP tx link
            rxLink = linkMap[mp][link-60]
            # find the frame index of the header of first MP tx packet
            txStart = data.findPackets(txBuf[link])[0][0]
            # find the frame index of the first header of a given demux tx packet
            # these are staggered according to the MP id
            rxStart = dmOffset + (mp*6)
            # loop over frames in the demux rx buffer file being constructed
            for frame in range(0,1024):
                # put commas at the start and end
                if frame < rxStart or (txStart+frame-rxStart) >= 1023:
                    rxBuf[rxLink][frame] = comma
                # if no valid data put commas
                elif not txBuf[link][txStart+frame-rxStart].valid:
                    rxBuf[rxLink][frame] = comma
                # first packet header has bit 13 set
                elif frame == rxStart:
                    firstHead.data = 0x00001000 | rxLink%6
                    rxBuf[rxLink][frame] = firstHead
                    event += 1
                # rest of the packet headers
                elif frame in [ rxStart + ev*54 for ev in range(1,18) ]:
                    head.data = event*len(txBuffers) + rxLink%6
                    rxBuf[linkMap[mp][link-60]][frame] = head
                    event += 1
                # payload
                else:
                    rxBuf[linkMap[mp][link-60]][frame] = txBuf[link][txStart+frame-rxStart]
            sys.stdout.write('.')
            sys.stdout.flush()

    print('')
    outFn = outDir + '/demux_rx_summary.txt'
    print('Saving Demux rx file to ' + outFn)
    mp7.BoardDataWriter.put(mp7.BoardDataWriter(outFn), rxBuf)


def readMPTxFiles(inputDir):

    print('Reading MP tx files from ' + inputDir + '...')

    txBuffers = []

    for mp in range(0,9):
        try:
            txFilename = inputDir + '/' + str(mp) + '/tx_summary.txt'
            tx = mp7.BoardDataFactory.readFromFile(txFilename)
            txBuffers.append(tx)
        except IOError:
            print('MP ' + str(mp) + ' tx file not found. Exiting...')
            sys.exit(1)

    print('Successfully read ' + str(len(txBuffers)) + ' MP tx buffers')

    return txBuffers
    


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--inputDir', help='Directory for MP rx files')
    parser.add_argument('--outputDir', help='Directory Demux for tx file', default='./')

    args = parser.parse_args()

    txFiles = readMPTxFiles(args.inputDir)

    generateDemuxRx(txFiles, args.outputDir)

    
