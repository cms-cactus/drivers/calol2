#!/bin/env python3.6

import argparse
import os
import sys
import math
import subprocess
import convertMPTxToDemuxRx

def call(cmdString):

    print(cmdString)
    subprocess.call(cmdString, shell=True)

def testMP(**kwargs):

    cf = kwargs['connectionsFile']
    bid = kwargs['boardID']
    outdir = kwargs['outDir']
    binFile = kwargs['mpBitfile'].split('.')[0] + '.bin'

    if kwargs['uploadFw'] == 'True':
        call('calol2butler.py -c ' + cf + ' uploadfw ' + bid + ' ' + kwargs['mpBitfile'] + ' ' + binFile)
    call('calol2butler.py -c ' + cf + ' rebootfpga ' + bid + ' ' + binFile)
    call('calol2butler.py -c ' + cf + ' reset ' + bid + ' --clksrc internal')
    call('calol2butler.py -c ' + cf + ' formatters ' + bid + ' --tdrfmt strip,insert')
    call('calol2butler.py -c ' + cf + ' funkyzeros ' + bid)
    call('calol2butler.py -c ' + cf + ' funkyrecovery ' + bid + ' mp -p ' + kwargs['mifFiles'])
    

    for mp in range(0,int(kwargs['nMP'])):
        call('calol2butler.py -c ' + cf + ' buffers ' + bid + ' algoPlay --inject file://' + kwargs['rxFiles'] + '/' + str(mp) + '/rx_summary.txt')
        call('calol2butler.py -c ' + cf + ' capture ' + bid + ' --out ' + outdir + '/' + str(mp) + '/')
        call('cat ' + outdir + '/' + str(mp) + '/rx_summary.txt >> ' + outdir + '/mp_rx_summary.txt')
        call('cat ' + outdir + '/' + str(mp) + '/tx_summary.txt >> ' + outdir + '/mp_tx_summary.txt')
        


def testDemux(**kwargs):

    cf = kwargs['connectionsFile']
    bid = kwargs['boardID']
    outdir = kwargs['outDir']
    binFile = kwargs['demuxBitfile'].split('.')[0] + '.bin'

    txFiles  = convertMPTxToDemuxRx.readMPTxFiles(outdir)
    convertMPTxToDemuxRx.generateDemuxRx(txFiles, outdir)
    
    if kwargs['uploadFw'] == 'True':
        call('calol2butler.py -c ' + cf + ' uploadfw ' + bid + ' ' + kwargs['demuxBitfile'] + ' ' + binFile)

    call('calol2butler.py -c ' + cf + ' rebootfpga ' + bid + ' ' + binFile)
    call('calol2butler.py -c ' + cf + ' reset ' + bid + ' --clksrc internal')
    call('calol2butler.py -c ' + cf + ' formatters ' + bid + ' --dmx-hdrfmt strip,insert')
    call('calol2butler.py -c ' + cf + ' funkyzeros ' + bid)
    call('calol2butler.py -c ' + cf + ' funkyrecovery ' + bid + ' demux -p ' + kwargs['mifFiles'])
    call('calol2butler.py -c ' + cf + ' xbuffers ' + bid + ' rx kPlayOnce --inject file://' + outdir + '/demux_rx_summary.txt')
    call('calol2butler.py -c ' + cf + ' xbuffers -e 4-11 ' + bid + ' tx kCapture')
    call('calol2butler.py -c ' + cf + ' capture ' + bid + ' --output ' + outdir + '/demux/')
    call('cp ' + outdir + '/demux/rx_summary.txt ' + outdir + '/demux_rx_summary.txt')
    call('cp ' + outdir + '/demux/tx_summary.txt ' + outdir + '/demux_tx_summary.txt')
    

    print('Now run the following in CMSSW: ')
    print('cmsRun EventFilter/L1TRawToDigi/utils/unpackBuffers-CaloStage2.py maxEvents=153 doMP=True doDemux=True doGT=False mpKeyLinkTx=60 mpLatency=84 debug=True nMP=9 fwVersion=0x10010033 dmLatency=23 dmOffset=13')
    print('cmsRun L1Trigger/L1TCalorimeter/test/runEmulator-CaloStage2.py inputFiles=file:l1tCalo_2016_EDM.root maxEvents=153 doLayer1=False')
    print('cmsRun L1Trigger/L1TCalorimeter/test/runCaloLayer2MPFWValidation.py inputFiles=file:l1tCalo_2016_simEDM.root')
    print('root -l -b -q \'compHwEmu.C(1,"hlt",1,0)\'')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--testMP', help='Option to test MP firmware', default=True)
    parser.add_argument('--testDemux', help='Option to test demux firmware', default=False)
    parser.add_argument('--mpBitfile', help='Main processor bitfile to test')
    parser.add_argument('--demuxBitfile', help='Demux bitfile to test')
    parser.add_argument('--boardID', help='BoardID from connections file')
    parser.add_argument('--nMP', help='Number of MP buffer files to run through MP firmware')
    parser.add_argument('--rxFiles', help='Location of MP input files')
    parser.add_argument('--mifFiles', help='Location of mif files to program MP and/or demux config')
    parser.add_argument('--outDir', help='Directory for buffer captures files (default = "./test/")', default='./test/')
    parser.add_argument('--connectionsFile', help='Connections file pointing to board')
    parser.add_argument('--uploadFw', help='Flag to upload firmware or not', default=True)
    args = parser.parse_args()

    if args.testMP == 'True' and not args.mpBitfile:
        print("Please provide an MP bitfile to test")
        sys.exit(1)
    if args.testDemux == 'True' and not args.demuxBitfile:
        print("Please provide a Demux bitfile to test")
        sys.exit(1)
    if not args.boardID:
        print("Please provide Board ID from connections file")
        sys.exit(1)
    if not args.rxFiles:
        print("Please provide MP rx files to test the board")
        sys.exit(1)
    if not args.mifFiles:
        print("Please provide location of mif files to configure the board for testing")
        sys.exit(1)
    if args.uploadFw == 'True' and args.testMP  == 'True' and not os.path.isfile(args.mpBitfile):
        print("Cannot find mp bitfile")
        sys.exit(1)
    if args.uploadFw == 'True' and args.testDemux == 'True' and not os.path.isfile(args.demuxBitfile):
        print("Cannot find demux bitfile")
        sys.exit(1)
    if False in [os.path.isfile(args.rxFiles + str(mp) + '/rx_summary.txt') for mp in range(0,int(args.nMP))]:
        print("Cannot find mp rx input buffer files")
        sys.exit(1)
    if not args.connectionsFile:
        print("Please provide a connections file")
        sys.exit(1)
    if os.path.isdir(args.outDir):
        print("Outdir exists. Please remove or specify another output destination")
        sys.exit(1)

    if args.testMP == 'True':
        testMP(**vars(args))
    if args.testDemux == 'True':
        testDemux(**vars(args))
