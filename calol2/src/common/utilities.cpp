#include "calol2/utilities.hpp"

// C++ Headers
#include <cmath>
#include <fstream>
#include <unordered_map>

// Boost Headers
#include <boost/tokenizer.hpp>

// MP7 Headers
#include "mp7/Logger.hpp"

namespace calol2 {
namespace utils {

// --------------------------------------------------------------------------------------------
std::vector< uint32_t > VectorToBLOB( const std::vector< uint32_t >& aVector , const uint32_t& aWidth )
{
  if ( aWidth > 32 )
  {
    return aVector;
  }

  // -------------------
  // The BLOB itself
  std::vector< uint32_t > lRet( ceil( aVector.size() * aWidth / 32.0 ) , 0x00000000 );
  // Variables to track of the word and bits in the BLOB to which we are writing
  std::vector< uint32_t >::iterator lWrite( lRet.begin() );
  int32_t lOffset( 0 );     
  // -------------------

  // -------------------
  // A mask to make sure the user isn't doing anything naughty or stupid
  uint32_t lMask( 0 );
  for( uint32_t i(0) ; i!=aWidth ; ++i ) lMask |= (0x1<<i);
  // -------------------

  // -------------------
  // The packing loop
  for( std::vector< uint32_t >::const_iterator lRead( aVector.begin() ) ; lRead != aVector.end() ; lRead++ )
  {
    *lWrite |= ( (*lRead & lMask) << (lOffset) );
    if( (lOffset += aWidth) > 31 ) *(++lWrite) |= ( (*lRead & lMask) >> ( aWidth - (lOffset -= 32) ) );
  }
  // -------------------

  return lRet;
}
// --------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------
std::vector< uint32_t > BLOBtoVector( const std::vector< uint32_t >& aBlob , const uint32_t& aWidth )
{
  if ( aWidth > 32 )
  {
    return aBlob;
  }

  // -------------------
  // The vector itself
  std::vector< uint32_t > lRet( floor( aBlob.size() * 32.0 / aWidth ) , 0x00000000 );

  // Variables to track of the word and bits in the BLOB which we are reading
  std::vector< uint32_t >::const_iterator lRead( aBlob.begin() );
  int32_t lOffset( 0 );     
  // -------------------

  // -------------------
  // A mask to select the right bits
  uint32_t lMask( 0 );
  for( uint32_t i(0) ; i!=aWidth ; ++i ) lMask |= (0x1<<i);
  // -------------------

  // -------------------
  // The unpacking loop
  for( std::vector< uint32_t >::iterator lWrite( lRet.begin() ) ; lWrite != lRet.end() ; lWrite++ )
  {
    *lWrite |= ( (*lRead >> lOffset) & (lMask) );
    if( (lOffset += aWidth) > 31 ) *lWrite |= ( ( (*(++lRead) ) << ( aWidth - (lOffset -= 32) ) ) & (lMask) );
  }
  // -------------------

  return lRet;
}
// --------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------
FMBusFileAccess::FMBusFileAccess(const std::unordered_map<std::string, std::string>& aMap, const std::string& aPath) :
mPath(aPath),
mMap(aMap)
{
}
// --------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------
void FMBusFileAccess::operator()(const std::string& aName, std::vector<uint32_t>& aData) const
{
  // --------------------------------
  static const boost::char_separator<char> lSeparators("/");
  boost::tokenizer< boost::char_separator<char> > lTokens(aName, lSeparators); // split endpoint name at '/' characters
  std::unordered_map< std::string, std::string >::const_iterator lIt(mMap.find(*lTokens.begin())); // associate endpoint with filename & size
  if (lIt == mMap.end()) {
    MP7_LOG(mp7::logger::kWarning) << "Cannot determine which file to use for FMB endpoint '" << aName << "'";
    return; // return nothing if we are not in the "database"
  }
  // --------------------------------
  // Split off line number suffix from end of filename
  const size_t lSuffixIdx = lIt->second.rfind(":");
  const int lRequestedLineIdx = (lSuffixIdx != std::string::npos) ? std::stoul(lIt->second.substr(lSuffixIdx+1)) : -1;
  
  boost::filesystem::path lPath(boost::filesystem::canonical(mPath / lIt->second.substr(0, lSuffixIdx))); // create path to file
  if (!boost::filesystem::is_regular_file(lPath))
    throw NoSuchFile(); // check file exists
  std::cout << aName << " -> " << *lTokens.begin() << " -> " << lIt->second << " -> " << lPath << std::endl;
  // --------------------------------
  std::string lLine;
  std::ifstream lFile(lPath.string().c_str());
  for (int i=0; i < lRequestedLineIdx; i++)
    lFile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  while (getline(lFile, lLine)) {
    aData.push_back(std::stoul(lLine, NULL, 16));
    if (lRequestedLineIdx >= 0) {
      std:: cout << "   (Only using line " << lRequestedLineIdx << " from this file)" << std::endl;
      break;
    }
  }
  lFile.close();
  std::cout << "File read successfully" << std::endl;
  // --------------------------------
}

const std::unordered_map<std::string, std::string> kFMBusMPFileMap({
  { "C", "C_EgammaCalibration_12to18.mif"},
  //{ "D", "D_EgammaIsolation_13to9.mif"},
  //{ "H1", "H_TauIsolation1_12to9.mif"},
  //{ "H2", "H_TauIsolation2_12to9.mif"},
  { "I", "I_TauCalibration_11to18.mif"},
  { "L", "L_JetCalibration_11to18.mif"},
  { "ME", "M_ETMETecal_11to18.mif"},
  { "MS", "M_ETMET_11to18.mif"},
  { "MX", "M_ETMETX_11to18.mif"},
  { "MY", "M_ETMETY_11to18.mif"},
  { "JetSeedThr", "1_JetSeedThreshold.mif"},
  { "ClstrSeedThr", "2_ClusterSeedThreshold.mif"},
  { "ClstrThr", "3_ClusterThreshold.mif"},
  { "PileUpThr", "4_PileUpThreshold.mif"},
  { "JetEtaMax", "6_JetEtaMax.mif"},
  { "EgammaEtaMax", "egammaMaxEta.mif"},
  { "TauEtaMax", "tauMaxEta.mif"},
  { "HT_Thr", "8_HtThreshold.mif"},
  { "MHT_Thr", "9_MHtThreshold.mif"},
  { "HTMHT_MaxEta", "HTMHT_maxJetEta.mif"},
  { "ETMET_MaxEta", "ETMET_maxTowerEta.mif"},
  { "EgRelaxThr", "10_EgRelaxThr.mif"},
  // ---
  { "HeavyIonThr", "HeavyIonThr.mif"},
  { "HeavyIonEta", "HeavyIonEta.mif"},
  { "BypassEgVeto", "BypassEgVeto.mif"},
  { "BypassJetPUS", "BypassJetPUS.mif"},
  // ---
  { "ET", "_EtThreshold.mif"},
  { "MET", "_MEtThreshold.mif"},
  { "ECAL", "_ECALEtThreshold.mif"},
  { "RatioCutLt15", "_RatioCutLt15.mif"},
  { "RatioCutGe15", "_RatioCutGe15.mif"},
  // --- LUTs added in (v61 to) v66
  { "BypassExtHE", "_BypassExtHE.mif"},
  { "D1", "D_EgammaIsolation1_13to9.mif"},
  { "D2", "D_EgammaIsolation2_13to9.mif"},
  { "H", "H_TauIsolation_12to9.mif"},
  { "X_ECAL_Thr", "X_EcalTHR_11to9.mif"},
  { "X_ET_Thr", "X_ETTHR_11to9.mif"},
  { "X_MET_Thr", "X_METTHR_11to9.mif"},
  // ---
  { "P", "P_TauTrimming_13to8.mif" },
  { "BypassEgShap", "BypassEgShape.mif"},
  { "BypassEcalFG", "BypassEcalFG.mif"},
  { "FatRingPUS", "PhiRingPUS.mif"},
});

const std::unordered_map<std::string, std::string> kFMBusDemuxFileMap({
  {"Centrality0L", "CentralityLowerThrs.mif:0"},
  {"Centrality1L", "CentralityLowerThrs.mif:1"},
  {"Centrality2L", "CentralityLowerThrs.mif:2"},
  {"Centrality3L", "CentralityLowerThrs.mif:3"},
  {"Centrality4L", "CentralityLowerThrs.mif:4"},
  {"Centrality5L", "CentralityLowerThrs.mif:5"},
  {"Centrality6L", "CentralityLowerThrs.mif:6"},
  {"Centrality7L", "CentralityLowerThrs.mif:7"},
  {"Centrality0H", "CentralityUpperThrs.mif:0"},
  {"Centrality1H", "CentralityUpperThrs.mif:1"},
  {"Centrality2H", "CentralityUpperThrs.mif:2"},
  {"Centrality3H", "CentralityUpperThrs.mif:3"},
  {"Centrality4H", "CentralityUpperThrs.mif:4"},
  {"Centrality5H", "CentralityUpperThrs.mif:5"},
  {"Centrality6H", "CentralityUpperThrs.mif:6"},
  {"Centrality7H", "CentralityUpperThrs.mif:7"},
  {"M_MET-HFcal", "M_METnoHFenergyCalibration_12to18.mif"},
  {"M_MET+HFcal", "M_METwithHFenergyCalibration_12to18.mif"},
  {"Q_MET-HFcal", "Q_METnoHFphiCalibration_12to18.mif"},
  {"Q_MET+HFcal", "Q_METwithHFphiCalibration_12to18.mif"},
  {"R_EcalCal", "R_EcalCalibration_12to18.mif"},
  {"S_ETcal", "S_ETcalibration_12to18.mif"}
});

FMBusFileAccess getFMBusMPFileReader(const std::string& aPath)
{
  return FMBusFileAccess(kFMBusMPFileMap, aPath);
}


FMBusFileAccess getFMBusDemuxFileReader(const std::string& aPath)
{
  return FMBusFileAccess(kFMBusDemuxFileMap, aPath);
}


AllZeros::AllZeros()
{
}


void AllZeros::operator()(const std::string& aName, std::vector<uint32_t>& aData) const
{
  MP7_LOG(mp7::logger::kDebug) << "Zeroing " << aName;
  static const std::vector< uint32_t > lZeros( 8192 , 0x00000000 ); //8192 @ width=9 = max RAM size at minimum ram width
  aData = lZeros;
}


} // namespace utils
} // namespace calol2
