
#include "pybind11/stl.h" // Automatically adds converters for STL collection/map classes
#include "pybind11/pybind11.h"

#include "calol2/FunkyMiniBus.hpp"
#include "calol2/CaloL2Controller.hpp"
#include "calol2/utilities.hpp"


namespace py = pybind11;


// *** N.B: The argument of this BOOST_PYTHON_MODULE macro MUST be the same as the name of the library created, i.e. if creating library file my_py_binds_module.so , imported in python as:
//                import my_py_binds_module
//          then would have to put
//                BOOST_PYTHON_MODULE(my_py_binds_module)
//          Otherwise, will get the error message "ImportError: dynamic module does not define init function (initmy_py_binds_module)
PYBIND11_MODULE(_pycalol2, m) {
  
  // FunkyMiniBus scoping (add calol2 namespace)
  auto lFunkyMiniBus = py::class_<calol2::FunkyMiniBus>(m, "FunkyMiniBus")
    .def(py::init<const uhal::Node&>())
    .def("__str__", [](const calol2::FunkyMiniBus& x) {std::ostringstream os; os << x; return os.str();})
    .def("__iter__", [](const calol2::FunkyMiniBus& x) { return py::make_iterator(x.begin(), x.end()); }, py::return_value_policy::reference_internal)
    .def("__len__", &calol2::FunkyMiniBus::size)
    .def("lock", &calol2::FunkyMiniBus::lock)
    .def("unlock", &calol2::FunkyMiniBus::unlock)
//      .def("autoConfigure", static_cast<void (calol2::FunkyMiniBus::*)(calol2::FunkyMiniBus::CallbackFn_t)>(&calol2::FunkyMiniBus::AutoConfigure))
    .def("autoConfigure", static_cast<void (calol2::FunkyMiniBus::*)(const calol2::FunkyMiniBus::CallbackFunctor&)>(&calol2::FunkyMiniBus::AutoConfigure))
    .def("readToFile", &calol2::FunkyMiniBus::ReadToFile);

    // Wrap FunkyMiniBus::Endpoint
  py::class_<calol2::FunkyMiniBus::Endpoint, std::shared_ptr<calol2::FunkyMiniBus::Endpoint>>(lFunkyMiniBus, "Endpoint")
    .def("__str__", [](const calol2::FunkyMiniBus::Endpoint& x) {std::ostringstream os; os << x; return os.str();})
    .def("size", &calol2::FunkyMiniBus::Endpoint::size)
    .def("width", &calol2::FunkyMiniBus::Endpoint::width)
    .def("name", &calol2::FunkyMiniBus::Endpoint::name, py::return_value_policy::copy)
    .def("read", &calol2::FunkyMiniBus::Endpoint::read)
    .def("write", &calol2::FunkyMiniBus::Endpoint::write)
    .def("lock", &calol2::FunkyMiniBus::Endpoint::lock);
    
  // Abstract -> noncopiable
  py::class_<calol2::FunkyMiniBus::CallbackFunctor>(lFunkyMiniBus, "CallbackFunctor");
  
  py::class_<calol2::utils::FMBusFileAccess, calol2::FunkyMiniBus::CallbackFunctor>(m, "FMBusFileAccess");
  m.def("getFMBusMPFileReader", &calol2::utils::getFMBusMPFileReader);
  m.def("getFMBusDemuxFileReader", &calol2::utils::getFMBusDemuxFileReader);

  py::class_<calol2::utils::AllZeros, calol2::FunkyMiniBus::CallbackFunctor>(m, "AllZeros")
    .def(py::init<>());

  py::class_<calol2::CaloL2Controller, mp7::MP7Controller>(m, "CaloL2Controller")
    .def(py::init<const uhal::HwInterface& >())
    .def("funkyMgr", &calol2::CaloL2Controller::funkyMgr, py::return_value_policy::reference_internal);
}
